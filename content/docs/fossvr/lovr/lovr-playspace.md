---
weight: 200
title: LÖVR Playspace
---

# lovr-playspace

- [LÖVR Playspace repository](https://github.com/SpookySkeletons/lovr-playspace)

> Room boundary overlay for OpenXR, made with LÖVR.

Avoid bumping into walls! Acquire a LÖVR appimage and execute it while providing an argument to the lovr-playspace directory location to utilize.

lovr-playspace works on any runtime that implements EXTX_OVERLAY. Currently this is limited to Monado-based runtimes.

WiVRn users: while WiVRn supports EXTX_OVERLAY, it does no frame composition on the server side, so overlay apps can have ghosting issues.

![Preview of the playspace in action.](https://github.com/SpookySkeletons/lovr-playspace/blob/main/assets/preview.png?raw=true)
